-module(tanodb).

-export([ping/0, get/1, put/2, delete/1, keys/1]).
-ignore_xref([ping/0, get/1, put/2, delete/1, keys/1]).

-define(N, 3).
-define(W, 3).

%% Public API

%% @doc Pings a random vnode to make sure communication is functional
ping() ->
    send_to_one({<<"ping">>, term_to_binary(os:timestamp())}, ping).

get(Key) ->
    ReqId = make_ref(),
    Timeout = 5000,
    tanodb_write_fsm:get(?N, Key, self(), ReqId),
    wait_for_reqid(ReqId, Timeout).

put(Key, Value) ->
    ReqId = make_ref(),
    Timeout = 5000,
    tanodb_write_fsm:put(?N, ?W, Key, Value, self(), ReqId),
    wait_for_reqid(ReqId, Timeout).

delete(Key) ->
    ReqId = make_ref(),
    Timeout = 5000,
    tanodb_write_fsm:delete(?N, Key, self(), ReqId),
    wait_for_reqid(ReqId, Timeout).

keys(Bucket) ->
    Timeout = 5000,
    tanodb_coverage_fsm:start({keys, Bucket}, Timeout).

%% Private Functions

send_to_one(Key, Cmd) ->
    DocIdx = riak_core_util:chash_key(Key),
    PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, tanodb),
    [{IndexNode, _Type}] = PrefList,
    riak_core_vnode_master:sync_spawn_command(IndexNode, Cmd, tanodb_vnode_master).

wait_for_reqid(ReqId, Timeout) ->
    receive
        {ReqId, {error, Reason}} -> {error, Reason};
        {ReqId, Val} -> Val
    after
        Timeout -> {error, timeout}
    end.
