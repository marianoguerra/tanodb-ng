-module(tanodb_kv_ets).
-export([new/1, get/2, put/3, delete/2, keys/2, dispose/1, delete/1,
         is_empty/1, foldl/3]).

-record(state, {table_id}).

new(_Opts) ->
    TableId = ets:new(?MODULE, [set, {write_concurrency, false},
                                {read_concurrency, false}]),
    State = #state{table_id=TableId},
    {ok, State}.

put(State=#state{table_id=TableId}, Key, Value) ->
    true = ets:insert(TableId, {Key, Value}),
    {ok, State}.

get(State=#state{table_id=TableId}, Key) ->
    Res = case ets:lookup(TableId, Key) of
              [] -> {not_found, Key};
              [{_, Value}] -> {found, {Key, Value}}
          end,
    {Res, State}.

delete(State=#state{table_id=TableId}, Key) ->
    true = ets:delete(TableId, Key),
    {ok, State}.

keys(State=#state{table_id=TableId}, Bucket) ->
    Keys0 = ets:match(TableId, {{Bucket, '$1'}, '_'}),
    Keys = lists:map(fun first/1, Keys0),
    {Keys, State}.

is_empty(State=#state{table_id=TableId}) ->
    IsEmpty = (ets:first(TableId) =:= '$end_of_table'),
    {IsEmpty, State}.

dispose(State=#state{table_id=TableId}) ->
    true = ets:delete_all_objects(TableId),
    {ok, State}.

delete(State=#state{table_id=TableId}) ->
    true = ets:delete(TableId),
    {ok, State}.

foldl(Fun, Acc0, State=#state{table_id=TableId}) ->
    AccOut = ets:foldl(Fun, Acc0, TableId),
    {AccOut, State}.

% private functions
first([V|_]) -> V.
