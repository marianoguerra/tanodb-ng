-module(tanodb_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    case tanodb_sup:start_link() of
        {ok, Pid} ->
            ok = riak_core:register([{vnode_module, tanodb_vnode}]),
            ok = riak_core_node_watcher:service_up(tanodb, self()),
            ok = setup_http_api(),

            {ok, Pid};
        {error, Reason} ->
            {error, Reason}
    end.

stop(_State) ->
    ok.

%% Private Functions

setup_http_api() ->
  Dispatch = cowboy_router:compile([{'_', [{"/kv/:bucket/:key", tanodb_h_kv, []}]}]),
  
  HttpPort = application:get_env(tanodb, http_port, 8080),
  HttpAcceptors = application:get_env(tanodb, http_acceptors, 100),
  HttpMaxConnections = application:get_env(tanodb, http_max_connections, infinity),

  lager:info("Starting HTTP API at port ~p", [HttpPort]),

  {ok, _} = cowboy:start_clear(tanodb_http_listener,
    [{port, HttpPort},
     {num_acceptors, HttpAcceptors},
     {max_connections, HttpMaxConnections}],
    #{env => #{dispatch => Dispatch}}),

  ok.
